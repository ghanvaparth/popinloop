<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContestHashTag extends Model
{
    protected $table='contest_hash_tags';
    protected $fillable = [
        'ContestID', 'HashTagID'
    ];
}
