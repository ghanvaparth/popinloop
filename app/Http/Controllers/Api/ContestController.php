<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use App\ContestModel;
use Str;

class ContestController extends Controller
{
    public function GetFreeContest(Request $request)
    {
        $userContest = $request->user()->FreeContest;
        if(!$userContest->isEmpty()){
        	return response()->json([
        	    'Success'=>true,
        	    'message' => 'Contest Found.',
        	    'data'=>$userContest
        	],201);
        }
        else{
        	return response()->json([
                'Success'=>false,
                'message' => 'Contest Not Found.'
            ], 401);
        }
    }
    public function GetPaidContest(Request $request)
    {
        $userContest = $request->user()->PaidContest;
        if(!$userContest->isEmpty()){
        	return response()->json([
        	    'Success'=>true,
        	    'message' => 'Contest Found.',
        	    'data'=>$userContest
        	],201);
        }
        else{
        	return response()->json([
                'Success'=>false,
                'message' => 'Contest Not Found.'
            ], 401);
        }
    }
}
