<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\Password;
use App\Notifications\EmailVerificationToken;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\PasswordReset;
use Str;

class AuthController extends Controller
{
    public function DoRegister(Request $request)
    {
        $request->validate([
            'FirstName' => 'required|string',
            'LastName' => 'required|string',
            'Gender' => 'required|string',
            'Email' => 'required|string|email|unique:users',
            'Password' => 'required|string|confirmed',
            'ProfilePic' => 'image|max:10000|mimes:jpeg,png,jpg,gif,svg'
        ]);
        $FileName = '';
        if($request->hasFile('ProfilePic'))
        {
        	$ProfilePic = $request->file('ProfilePic');
        	$FileName = time().$ProfilePic->getClientOriginalName();
        	$ProfilePic->move(public_path().'/uploads/ProfilePic/', $FileName);
        }
        $token = Str::random(30);
        $user = User::create([
		    'FirstName' => $request->FirstName,
	        'LastName' => $request->LastName,
	        'email' => $request->Email,
	        'password' => bcrypt($request->Password),
	        'Age' => $request->Age,
	        'Gender' => $request->Gender,
	        'Upi' => $request->UPI,
	        'ProfilePic' => $FileName,
            'EmailVerificationToken' => $token,
            'Roles' => 2,
		]);
        $user->assignRole('Customer');
        
        $user->notify(
            new EmailVerificationToken($token)
        );
        return response()->json([
        	'Success' => true,
            'message' => 'Register Successfully!'
        ], 201);
    }

    public function DoLogin(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);
        $credentials = request(['email', 'password']);

        if(!Auth::attempt($credentials))
            return response()->json([
                'Success'=>false,
                'message' => 'Unauthorized'
            ], 401);

        $user = $request->user();
        if($user->email_verified_at == '' || $user->email_verified_at == NULL)
        {
            return response()->json([
                'Success'=>false,
                'message' => 'Your email address is not verified.'
            ], 401);
        }
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->device_token =$request->device_token;
        $token->imei =$request->imei;
        $token->device_name =$request->device_name;
        $token->os_version =$request->os_version;
        $token->save();

        return response()->json([
            'Success'=>true,
            'message'=>'registered successfully.',
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'user_data'=>$user,
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'Success'=>true,
            'message' => 'Successfully logged out'
        ],201);
    }
    public function ForgotPassword(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
        ]);
        $user = User::where('email', $request->email)->first();
        if(!$user)
            return response()->json([
                'Success'=>false,
                'message' => 'We can\'t find a user with that e-mail address.'
            ], 404);
        $token_reset = Password::broker()->createToken($user);
        if($user)
            $user->notify(
                new PasswordResetRequest($token_reset,$user->email)
            );
        return response()->json([
            'Success'=>true,
            'message' => 'We have e-mailed your password reset link!'
        ]);
    }
    public function VerifyEmail($email,$token)
    {
        $user = User::where('email', $email)->where('EmailVerificationToken', $token)->first();
        if($user)
        {
            $user->EmailVerificationToken = '';
            $user->email_verified_at = Carbon::now();
            $user->save();
            return view('emails.verify');
        }
        else
        {
            echo '<br><center><h1>Link Expired.</h1></center>';
        }
    }
    
}
