<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use App\User;
use Str;

class UserController extends Controller
{
    public function GetUserProfile(Request $request)
    {
       	if($request->UserProfileId>0)
    	{
    		$userProfileData=User::where('id',$request->UserProfileId)->first();
    	}
    	else{
    		$userProfileData=$request->user();
    	}
    	if ($userProfileData) {
    	return response()->json([
                'Success'=>true,
                'user_data'=>$userProfileData,
                'message' => 'Success.'
            ], 201);
    	 }
    	 else{
    	 	return response()->json([
                'Success'=>false,
                'message' => 'User Not Found.'
            ], 401);
    	 }
    }
    public function EditProfile(Request $request)
    {
    	$userProfileData=$request->user();
    	if($userProfileData->email!=$request->Email)
    	{
    		$unique_validation='|unique:users';
    	}
    	else{
    		$unique_validation="";
    	}
    	$request->validate([
            'FirstName' => 'required|string',
            'LastName' => 'required|string',
            'Email' => 'required|string|email'.$unique_validation.'',
            'ProfilePic' => 'image|max:10000|mimes:jpeg,png,jpg,gif,svg',
            'ProfileVideo' => 'mimetypes:video/avi,video/mpeg,video/mp4,video/mp3,video/quicktime',
            'DOB'=>['before:5 years ago']
        ]);
    	$ImageFileName=$userProfileData->profile_pic;
        if($request->hasFile('ProfileImage'))
        {
        	$ProfilePic = $request->file('ProfileImage');
        	$ImageFileName = time().$ProfilePic->getClientOriginalName();
        	$ProfilePic->move(public_path().'/uploads/ProfilePic/', $ImageFileName);
        }
        $VideoFileName=$userProfileData->profile_vid;
        if($request->hasFile('ProfileVideo'))
        {
        	$ProfileVideo = $request->file('ProfileVideo');
        	$VideoFileName = time().$ProfileVideo->getClientOriginalName();
        	$ProfileVideo->move(public_path().'/uploads/ProfileVideo/', $VideoFileName);
        }

        user::where('id', $userProfileData->id)->update(['ProfilePic'=>$ImageFileName,'ProfileVid'=>$VideoFileName,'LastName'=>$request->LastName,'FirstName' => $request->FirstName,'BirthDate'=>date('Y-m-d',strtotime($request->DOB)),'email'=>$request->Email,'WorkAt'=>$request->WorkAt,'Relationship'=>$request->Relationship,'Hobbies'=>$request->Hobbies,'Address'=>$request->Address,'MobileNumber'=>$request->MobileNo,'Status'=>$request->Status,'Biodata'=>$request->Bio]);
        return response()->json([
                'Success'=>true,
                'message' => 'Profile updated successfully.'
            ], 201);
         


    }

}
