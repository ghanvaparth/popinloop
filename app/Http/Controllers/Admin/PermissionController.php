<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Validator;
use Auth;
use DataTables;

class PermissionController extends Controller{

    public function index(Request $request){
        return view('admin.permission.index');
    }

    public function datatable(Request $request){
    	$permission = Permission::orderBy('id','DESC');
    	return DataTables::of($permission)->addColumn('action', function($permission) {
    		$edit_link="";
            //$delete_link="";
            if(Auth::user()->can('edit-permission')){
                $edit_link = '<a href="'.route('admin.permission.edit',$permission->id).'" data-toggle="tooltip" data-original-title="Edit"><i class="fas fa-pencil-alt text-inverse m-r-10"></i> </a>';
            }
            return $edit_link;
            
        })->make(true);
    }

    public function create(){
        return view('admin.permission.create');
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:permissions',
        ]);
        if ($validator->fails()) {
            return redirect()->route('admin.permission.add')
                        ->withErrors($validator)
                        ->withInput();
        }
        $permission     = Permission::create(['name' => Str::slug($request->name, "-"),'guard_name'=>'web','display_name'=>$request->name]);
        return redirect()->route('admin.permission.list')->with('success','Permission Added successfully.');
    }

    public function edit($id){
        $permission = Permission::where('id',$id)->first();
        if($permission){
            return view('admin.permission.edit', compact('permission'));
        }else{
            return abort(404); 
        }    
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:permissions,display_name,'.$id
        ]);
        if ($validator->fails()) {
            return redirect()->route('admin.permission.edit',$id)->withErrors($validator)->withInput();
        }
        $permission = Permission::where('id',$id)->first();
        $permission->display_name   = $request->name;
        $permission->save();
        return redirect()->route('admin.permission.list')->with('success','Permission updated successfully.');
    }

}
