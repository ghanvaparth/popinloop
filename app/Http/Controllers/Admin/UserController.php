<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\User;
use DB;
use Validator;
use DataTables;

class UserController extends Controller{

    public function index(Request $request){
        return view('admin.users.index');
    }

    public function datatable(Request $request){
        $user = User::select(['id','Roles',DB::raw("CONCAT(users.FirstName,' ',users.LastName) as fullname"),'email','IsActive','created_at'])->with('role');
        return Datatables::eloquent($user)
            ->addColumn('action', function($user) {
                $edit_link = '<a href="'.route('admin.users.edit',$user->id).'" data-toggle="tooltip" data-original-title="Edit"><i class="fas fa-pencil-alt text-inverse m-r-10"></i> </a>';
                return $edit_link;
            })
            ->addColumn('role', function (User $user) {
                return $user->role->name;
            })
            ->addColumn('IsActive', function($user) {
                if($user->IsActive == 0){
                    return '<a href="javascript:void(0);" class="btn btn-danger" data-title="Active" onclick="changestatus(this,'.$user->id.',1);">InActive</a>';
                }else{
                    return '<a href="javascript:void(0);" class="btn btn-success" data-title="InActive" onclick="changestatus(this,'.$user->id.',0);">Active</a>';
                }
            })
            ->editColumn('created_at', function($user) {
                return date('d-m-Y', strtotime($user->created_at));
            })
            ->filterColumn('fullname', function($query, $keyword) {
                $sql = "CONCAT(users.FirstName,'-',users.LastName)  like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->filterColumn('IsActive', function ($query, $keyword) {
                if (strstr(strtolower("active"), strtolower($keyword) ) !== false) {
                    $query->whereRaw("IsActive = 1");
                }elseif (strstr(strtolower("inactive"), strtolower($keyword) ) !== false) {
                    $query->whereRaw("IsActive = 0");
                }
            })
            ->filterColumn('created_at', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(created_at,'%d-%m-%Y') like ?", ["%$keyword%"]);
            })
            ->rawColumns(['IsActive','action'])
            ->make(true);
    }

    public function edit($id){
        $user = User::where('id',$id)->first();
        if($user){
            $roles = Role::all();
            return view('admin.users.edit', compact('user','roles'));
        }else{
            return abort(404); 
        }
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'role' => 'required',
            'FirstName' => 'required',
            'LastName' => 'required',
            'email' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return redirect()->route('admin.users.edit',$id)->withErrors($validator)->withInput();
        }
        $role = Role::findById($request->role);
        $user = User::where('id',$id)->first();
        $user->FirstName        = $request->FirstName;
        $user->LastName         = $request->LastName;
        $user->email            = $request->email;
        $user->Age              = $request->Age;
        $user->Gender           = $request->Gender;
        $user->syncRoles($role->name);
        $user->save();
        return redirect()->route('admin.users.list')->with('success','User updated successfully');
    }

    public function changestatus(Request $request) {
        $response['success'] = false;
        if ($request->ajax()) {
            try {
                User::where('id',$request->id)->update(['IsActive'=>$request->status]);
                $response['success'] = true;
            } catch (Exception $e) {
                $response['success'] = false;
            }
        }
        return response()->json($response);
    }

}
