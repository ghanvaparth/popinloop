<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Auth;
use DataTables;
use DB;

class RoleController extends Controller{
    
    public function index(Request $request){
        return view('admin.roles.index');
    }

    public function datatable(Request $request){
    	$roles = Role::orderBy('id','DESC');
    	return DataTables::of($roles)->addColumn('action', function($roles) {
            $edit_link="";
            $delete_link="";
            if(Auth::user()->can('edit-roles')){
                $edit_link = '<a href="'.route('admin.role.edit',$roles->id).'" data-toggle="tooltip" data-original-title="Edit"><i class="fas fa-pencil-alt text-inverse m-r-10"></i> </a>';
            }
            if(Auth::user()->can('delete-roles')){
                $delete_link = '<a href="javascript:void(0);" onclick="delete_confirmation(this,'.$roles->id.')" data-toggle="tooltip" data-original-title="Delete"><i class="fas fa-window-close text-danger"></i> </a>';
            }
            return $edit_link.$delete_link;
        })->make(true);
    }

    public function create(){
        $permission = Permission::get();
        return view('admin.roles.create',compact('permission'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
        ]);
        $role = Role::create(['name' => $request->input('name'),'description' => $request->input('name'),'page_permission'=>$request->input('name')]);
        $role->syncPermissions($request->input('permission'));
        return redirect()->route('admin.role.list')->with('success','Role created successfully');
    }

    public function edit($id){
        $role = Role::find($id);
        $permission = Permission::get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();
        return view('admin.roles.edit',compact('role','permission','rolePermissions'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',
        ]);
        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();
        $role->syncPermissions($request->input('permission'));
        return redirect()->route('admin.role.list')->with('success','Role updated successfully');
    }

    public function destroy(Request $request){
        Role::where('id',$request->id)->delete();
        return response()->json(['succsess'=>true]);
    }

}
