<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Contest;
use App\Hashtags;
use App\ContestHashTag;
use Auth;
use DB;
use Validator;
use DataTables;
use Redirect;

class ContestController extends Controller
{
    public function index(Request $request){
        return view('admin.contests.index');
    }
    public function datatable(Request $request){
    	$contest = Contest::withCount('ContestUser')->orderBy('Id','DESC')->get();
    	
    	return DataTables::of($contest)->addColumn('action', function($contest) {
            $edit_link="";
            $delete_link="";
            if(Auth::user()->can('edit-contests')){
                $edit_link = '<a href="'.route('admin.contests.edit',$contest->Id).'" data-toggle="tooltip" data-original-title="Edit"><i class="fas fa-pencil-alt text-inverse m-r-10"></i> </a>';
            }
            if(Auth::user()->can('delete-contests')){
                $delete_link = '<a href="javascript:void(0);" onclick="delete_confirmation(this,'.$contest->Id.')" data-toggle="tooltip" data-original-title="Delete"><i class="fas fa-window-close text-danger"></i> </a>';
            }
            return $edit_link.$delete_link;
        })->make(true);
    }
    public function create(){
        return view('admin.contests.create');
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ContestReffrenceImageUrl' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'ContestType'=>'required',
            'ContestTitle'=>'required',
            'RewardType'=>'required',
            'RewardPoints'=>'required|numeric',
            'PremiumRewardPoints'=>'required|numeric',
            'StartTime'=>'required',
        ]);

        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        $FileName = '';
        if($request->hasFile('ContestReffrenceImageUrl'))
        {
            $ProfilePic = $request->file('ContestReffrenceImageUrl');
            $FileName = time().$ProfilePic->getClientOriginalName();
            $ProfilePic->move(public_path().'/uploads/ContestReffrenceImage/', $FileName);
        }
        if($request->IsPaid=='on')
        {
            $IsPaid='1';
        }
        else{
            $IsPaid='0';
        }
        $explode_duration=explode(' - ', $request->StartTime);
        $ContestData = Contest::create([
            'ContestType' => $request->ContestType,
            'UserId'=>'1',
            'ContestTitle' => $request->ContestTitle,
            'RewardType' => $request->RewardType,
            'GuestRewardPoints' => $request->RewardPoints,
            'PremiumRewardPoints' => $request->PremiumRewardPoints,
            'ContestReffrenceImageUrl' => $FileName,
            'IsActive' => '1',
            'StartTime'=>date('Y-m-d H:i:s',strtotime($explode_duration[0])),
            'EndTime'=>date('Y-m-d H:i:s',strtotime($explode_duration[1])),
            'IsPaid' => $IsPaid
        ]);
        $ContestId=$ContestData->id;
        if(!empty($request->has_tags))
        {
            foreach($request->has_tags as $has_tags_val)
            {
                if(is_numeric($has_tags_val))
                {
                    ContestHashTag::create([
                        'ContestID'=>$ContestId,
                        'HashTagID'=>$has_tags_val
                        ]);
                }
                else{
                    $hashTagData=Hashtags::create([
                        'HashTagName'=>$has_tags_val,
                        'IsActive'=>'1',
                        ]);
                    $has_tag_id=$hashTagData->id;
                    ContestHashTag::create([
                        'ContestID'=>$ContestId,
                        'HashTagID'=>$has_tag_id
                        ]);
                }
            }
        }
        return redirect()->route('admin.contests.list')->with('success','Contest created successfully');

    }
    public function edit($id){
        $ContestData = Contest::find($id);
        return view('admin.contests.edit',compact('ContestData'));
    }
    public function hastags(Request $request)
    {
        $get_all_hashtag=Hashtags::where('HashTagName', 'like', '%'.$request->q.'%')->get();
        $search_user_arr = array();
        if(!empty($get_all_hashtag))
        {
            foreach ($get_all_hashtag as $key => $user) {
                $search_user_arr[$key]['text'] = $user['HashTagName'];
                $search_user_arr[$key]['id'] = $user['Id'];
            }
        }
        return response()->json(['response'=>$search_user_arr]);
    }
    public function destroy(Request $request){
        $ContestData = Contest::find($request->id);
        if(file_exists(public_path().'/uploads/ContestReffrenceImage/'.$ContestData->ContestReffrenceImageUrl)){
            @unlink(public_path().'/uploads/ContestReffrenceImage/'.$ContestData->ContestReffrenceImageUrl);
        }
        ContestHashTag::where('ContestID',$request->id)->delete();
        Contest::where('id',$request->id)->delete();
        return response()->json(['succsess'=>true]);
    }
}
