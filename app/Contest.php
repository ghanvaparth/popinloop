<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contest extends Model
{
	protected $table='contests';
    protected $fillable = [
        'UserId', 'ContestType', 'ContestTitle', 'StartTime' ,'EndTime' ,'RewardType' ,'GuestRewardPoints' ,'PremiumRewardPoints','ContestReffrenceImageUrl','IsActive','IsPaid','created_at','updated_at'
    ];
    public function ContestUser()
    {
    	return $this->hasMany('App\ContestUser','ContestId','Id');
    }
}
