<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens,Notifiable,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'FirstName', 'LastName', 'email', 'password' ,'Age' ,'Gender' ,'Upi' ,'ProfilePic','remember_token','created_at','updated_at','ProfileVid','MobileNumber','BirthDate','WorkAt','Relationship','Hobbies','Address','Status','Biodata','EmailVerificationToken','Roles'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function FreeContest(){
        return $this->hasMany('App\Contest', 'UserId', 'id')->where([['IsPaid','=', 0],['IsActive','=',1]]);
    }

    public function PaidContest(){
        return $this->hasMany('App\Contest', 'UserId', 'id')->where([['IsPaid','=', 1],['IsActive','=',1]]);   
    }

    public function role(){
        return $this->hasOne(Role::class,'id','Roles');
    }
}
