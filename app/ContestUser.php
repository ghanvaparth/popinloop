<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContestUser extends Model
{
    protected $table='contestusers';
    protected $fillable = [
        'ContestId', 'ParticipantUserId','IsActive','IsBanned','BannedReason'
    ];
}
