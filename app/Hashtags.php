<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hashtags extends Model
{
    protected $table='hash_tags';
    protected $fillable = [
        'HashTagName','IsActive','created_at','updated_at'
    ];
}
