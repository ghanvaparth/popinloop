<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Auth::routes(['verify' => true]);
Route::group([
    'namespace' => 'Api',
    'middleware' => 'ApiRequest'
], function () {
	Route::post('DoLogin', 'AuthController@DoLogin');
	Route::post('DoRegister', 'AuthController@DoRegister');
    Route::post('ForgotPassword', 'AuthController@ForgotPassword');
    Route::group([
      'middleware' => ['auth:api','APIToken']
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
        Route::post('GetUserProfile', 'UserController@GetUserProfile');
        Route::post('EditProfile', 'UserController@EditProfile');
        Route::get('GetFreeContest', 'ContestController@GetFreeContest');
        Route::get('GetPaidContest', 'ContestController@GetPaidContest');
    });
});
Route::get('email/verify/{id}/{hash}','Api\AuthController@VerifyEmail');
