<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['verify' => true]);
Route::get('/admin', 'Admin\LoginController@showlogin')->name('login1');
Route::get('/admin/login', 'Admin\LoginController@showlogin')->name('login2');
Route::group(['namespace' => 'Admin','as' => 'admin.','prefix'=>'admin','middleware' => 'auth'], function () {
	Route::get('/dashboard', 'DashboardController@index')->name('home');

	Route::group(['as' => 'role.','prefix'=>'role'], function () {
		Route::get('/', 'RoleController@index')->name('list');
		Route::post('/datatable', 'RoleController@datatable')->name('data');
		Route::get('/add', 'RoleController@create')->name('add');
		Route::post('/store', 'RoleController@store')->name('store');
		Route::post('/destroy', 'RoleController@destroy')->name('destroy');
		Route::get('/edit/{id}', 'RoleController@edit')->name('edit');
		Route::post('/update/{id}', 'RoleController@update')->name('update');
	});
	Route::group(['as' => 'permission.','prefix'=>'permission'], function () {
		Route::get('/', 'PermissionController@index')->name('list');
		Route::post('/datatable', 'PermissionController@datatable')->name('data');
		Route::get('/add', 'PermissionController@create')->name('add');
		Route::post('/store', 'PermissionController@store')->name('store');
		Route::get('/edit/{id}', 'PermissionController@edit')->name('edit');
		Route::post('/update/{id}', 'PermissionController@update')->name('update');
	});

	Route::group(['as' => 'users.','prefix' => 'users'], function () {
	    Route::get('/', 'UserController@index')->name('list');
	    Route::post('/datatable', 'UserController@datatable')->name('data');
	    Route::get('/edit/{id}', 'UserController@edit')->name('edit');
		Route::post('/update/{id}', 'UserController@update')->name('update');
		Route::post('/changestatus', 'UserController@changestatus')->name('changestatus');
	});

	Route::group(['as' => 'contests.','prefix' => 'contests'], function () {
	    Route::get('/', 'ContestController@index')->name('list');
	    Route::post('/datatable', 'ContestController@datatable')->name('data');
	    Route::get('/add', 'ContestController@create')->name('add');
	    Route::post('/store', 'ContestController@store')->name('store');
	    Route::get('/edit/{id}', 'ContestController@edit')->name('edit');
		Route::post('/update/{id}', 'ContestController@update')->name('update');
		Route::post('/hastags', 'ContestController@hastags')->name('hastags');
		Route::post('/destroy', 'ContestController@destroy')->name('destroy');
	});
});
