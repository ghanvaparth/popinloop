<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'Admin',
            'description' => 'Admin User',
            'page_permission' => 'all',
            'guard_name' => 'web'
        ]);

        DB::table('roles')->insert([
            'name' => 'Customer',
            'description' => 'Customer',
            'page_permission' => 'customer',
            'guard_name' => 'web'
        ]);
        
        $role = Role::findByName('Admin');
        $permissions = Permission::all();
        $role->syncPermissions($permissions);
    }
}
