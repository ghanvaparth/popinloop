<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
	            'name' 			=> 'add-users',
	            'display_name' 	=> 'Add User',
	            'guard_name' 	=> 'web',
	            'created_at'	=> date('Y-m-d H:i:s'),
	            'updated_at'	=> date('Y-m-d H:i:s')
	        ],
	        [
	            'name' 			=> 'edit-users',
	            'display_name' 	=> 'Edit User',
	            'guard_name' 	=> 'web',
	            'created_at'	=> date('Y-m-d H:i:s'),
	            'updated_at'	=> date('Y-m-d H:i:s')
	        ],
	        [
	            'name' 			=> 'delete-users',
	            'display_name' 	=> 'Delete User',
	            'guard_name' 	=> 'web',
	            'created_at'	=> date('Y-m-d H:i:s'),
	            'updated_at'	=> date('Y-m-d H:i:s')
	        ],
	        [
	            'name' 			=> 'list-users',
	            'display_name' 	=> 'List User',
	            'guard_name' 	=> 'web',
	            'created_at'	=> date('Y-m-d H:i:s'),
	            'updated_at'	=> date('Y-m-d H:i:s')
	        ],
	        [
	            'name' 			=> 'add-roles',
	            'display_name' 	=> 'Add Role',
	            'guard_name' 	=> 'web',
	            'created_at'	=> date('Y-m-d H:i:s'),
	            'updated_at'	=> date('Y-m-d H:i:s')
	        ],
	        [
	            'name' 			=> 'edit-roles',
	            'display_name' 	=> 'Edit Role',
	            'guard_name' 	=> 'web',
	            'created_at'	=> date('Y-m-d H:i:s'),
	            'updated_at'	=> date('Y-m-d H:i:s')
	        ],
	        [
	            'name' 			=> 'delete-roles',
	            'display_name' 	=> 'Delete Role',
	            'guard_name' 	=> 'web',
	            'created_at'	=> date('Y-m-d H:i:s'),
	            'updated_at'	=> date('Y-m-d H:i:s')
	        ],
	        [
	            'name' 			=> 'list-roles',
	            'display_name' 	=> 'List Role',
	            'guard_name' 	=> 'web',
	            'created_at'	=> date('Y-m-d H:i:s'),
	            'updated_at'	=> date('Y-m-d H:i:s')
	        ]
        ]);
    }
}
