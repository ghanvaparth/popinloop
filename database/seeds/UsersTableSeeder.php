<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin                   	= new User();
        $admin->FirstName          = 'Admin';
        $admin->LastName          	= 'User';
        $admin->email            	= 'admin@popinloop.com';
        $admin->password         	= bcrypt('admin1234');
        $admin->email_verified_at	= date("Y-m-d H:i:s");
        $admin->created_at       	= date("Y-m-d H:i:s");
        $admin->updated_at       	= date("Y-m-d H:i:s");
        $admin->Roles               = '1';
        $admin->save();
        $admin->assignRole('Admin');
    }
}
