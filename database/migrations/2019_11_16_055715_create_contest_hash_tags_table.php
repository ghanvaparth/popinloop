<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContestHashTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contest_hash_tags', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ContestID');
            $table->foreign('ContestID')->references('Id')->on('contests');
            $table->unsignedBigInteger('HashTagID');
            $table->foreign('HashTagID')->references('Id')->on('hash_tags');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contest_hash_tags');
    }
}
