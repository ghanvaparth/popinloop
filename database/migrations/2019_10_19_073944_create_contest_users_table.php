<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContestUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ContestUsers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ContestId');
            $table->unsignedBigInteger('ParticipantUserId');
            $table->foreign('ParticipantUserId')->references('id')->on('users');
            $table->foreign('ContestId')->references('Id')->on('contests');
            $table->integer('IsActive')->comment('1-active,0-deactive');
            $table->integer('IsBanned')->comment('1-banned,0-not banned');
            $table->text('BannedReason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ContestUsers');
    }
}
