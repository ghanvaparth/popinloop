<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMoreFieldToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('ProfileVid')->nullable();
            $table->string('MobileNumber',20)->nullable();
            $table->date('BirthDate')->nullable();
            $table->Text('WorkAt')->nullable();
            $table->Text('Relationship')->nullable();
            $table->Text('Hobbies')->nullable();
            $table->Text('Address')->nullable();
            $table->Text('Status')->nullable();
            $table->Text('Biodata')->nullable();
            $table->Text('EmailVerificationToken')->nullable();
            $table->integer('IsActive')->default(0);
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['ProfileVid','MobileNumber','BirthDate','WorkAt','Relationship','Hobbies','Address','Status','Biodata','EmailVerificationToken','IsActive']);

        });
    }
}
