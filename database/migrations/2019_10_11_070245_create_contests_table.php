<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contests', function (Blueprint $table) {
            $table->bigIncrements('Id');
            $table->unsignedBigInteger('UserId');
            $table->foreign('UserId')->references('id')->on('users');
            $table->integer('ContestType')->nullable()->comment('0-Audio ,1-Video ,2-Writing');
            $table->string('ContestTitle')->nullable();
            $table->dateTime('StartTime')->nullable();
            $table->dateTime('EndTime')->nullable();
            $table->integer('RewardType')->nullable()->comment('0-ruppies,1-point');
            $table->decimal('GuestRewardPoints',12,3)->nullable();
            $table->decimal('PremiumRewardPoints',12,3)->nullable();
            $table->string('ContestReffrenceImageUrl')->nullable();
            $table->integer('IsActive')->nullable()->comment('1-active,0-deactive');
            $table->integer('IsPaid')->nullable()->comment('0-free,1-paid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contests');
    }
}
