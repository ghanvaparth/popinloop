<!DOCTYPE html>
<html lang="en">
	<head>
		<title>PopinLoop</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!--===============================================================================================-->	
		<link rel="icon" type="image/png" href="{{asset('Auth/images/icons/favicon.ico')}}"/>
		<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{asset('Auth/vendor/bootstrap/css/bootstrap.min.css')}}">
		<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{asset('Auth/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
		<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{asset('Auth/fonts/iconic/css/material-design-iconic-font.min.css')}}">
		<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{asset('Auth/vendor/animate/animate.css')}}">
		<!--===============================================================================================-->	
		<link rel="stylesheet" type="text/css" href="{{asset('Auth/vendor/css-hamburgers/hamburgers.min.css')}}">
		<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{asset('Auth/vendor/animsition/css/animsition.min.css')}}">
		<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{asset('Auth/vendor/select2/select2.min.css')}}">
		<!--===============================================================================================-->	
		<link rel="stylesheet" type="text/css" href="{{asset('Auth/vendor/daterangepicker/daterangepicker.css')}}">
		<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{asset('Auth/css/util.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('Auth/css/main.css')}}">
		<!--===============================================================================================-->
		<style type="text/css">.invalid-feedback{ display: block;}</style>
	</head>
	<body>
		<div class="limiter">
			<div class="container-login100">
				<div class="wrap-login100">
					<form class="login100-form validate-form" action="{{ route('login') }}" method="POST">
					 	@csrf
						<span class="login100-form-title p-b-26">
							{{env('APP_NAME')}}
						</span>
						<span class="login100-form-title p-b-48">
						</span>
						<div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
							<input class="input100" type="text" name="email" autocomplete="email" autofocus value="{{ old('email') }}">
							<span class="focus-input100" data-placeholder="Email"></span>
							@error('email')
		                    <span class="invalid-feedback" role="alert">
		                        <strong>{{ $message }}</strong>
		                    </span>
		                	@enderror
						</div>
						<div class="wrap-input100 validate-input" data-validate="Enter password">
							<span class="btn-show-pass">
								<i class="zmdi zmdi-eye"></i>
							</span>
							<input class="input100" type="password" name="password" autocomplete="current-password">
							<span class="focus-input100" data-placeholder="Password"></span>
							@error('password')
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $message }}</strong>
	                            </span>
	                        @enderror
						</div>
						<div class="container-login100-form-btn">
							<div class="wrap-login100-form-btn">
								<div class="login100-form-bgbtn"></div>
								<button class="login100-form-btn">
									Login
								</button>
							</div>
						</div>
						<!-- <div class="text-center p-t-115">
							<span class="txt1">
								Don’t have an account?
							</span>
							<a class="txt2" href="#">
								Sign Up
							</a>
						</div> -->
					</form>
				</div>
			</div>
		</div>
		<div id="dropDownSelect1"></div>
		<!--===============================================================================================-->
		<script src="{{asset('Auth/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
		<!--===============================================================================================-->
		<script src="{{asset('Auth/vendor/animsition/js/animsition.min.js')}}"></script>
		<!--===============================================================================================-->
		<script src="{{asset('Auth/vendor/bootstrap/js/popper.js')}}"></script>
		<script src="{{asset('Auth/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
		<!--===============================================================================================-->
		<script src="{{asset('Auth/vendor/select2/select2.min.js')}}"></script>
		<!--===============================================================================================-->
		<script src="{{asset('Auth/vendor/daterangepicker/moment.min.js')}}"></script>
		<script src="{{asset('Auth/vendor/daterangepicker/daterangepicker.js')}}"></script>
		<!--===============================================================================================-->
		<script src="{{asset('Auth/vendor/countdowntime/countdowntime.js')}}"></script>
		<!--===============================================================================================-->
		<script src="{{asset('Auth/js/main.js')}}"></script>
	</body>
</html>