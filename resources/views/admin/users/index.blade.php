@extends('include.admin_app')
@section('metatitle')
    PopinLoop User
@endsection

@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">User List</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">User List</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col s12">
            <!-- Column -->
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table striped m-b-20" id="DataTable1">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Role</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{asset('assets/plugins/jquery-datatables-editable/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/media/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('assets/plugins/tiny-editable/mindmup-editabletable.js')}}"></script>
<script src="{{asset('assets/plugins/tiny-editable/numeric-input-example.js')}}"></script>
@if(Session::has('success'))
<script type="text/javascript">
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });
    Toast.fire({
        type: 'success',
        title: '{!! Session::get("success") !!}'
    });
</script>
@endif
<script>
    $(function() {
        var table=$('#DataTable1').DataTable({
            processing: true,
            serverSide: true,
            aoColumnDefs: [{ "bSortable": false, "aTargets": [6] }],
            ajax:{
                headers: {'X-CSRF-Token': Laravel.csrfToken},
                url:'{!! route("admin.users.data") !!}',
                type:'POST'
            },
            columns: [
                {data: 'id',name: 'id'},
                {data: 'role',name: 'role.name'},
                {data: 'fullname',name: 'fullname'},
                {data: 'email',name: 'email'},
                {data: 'IsActive', name: 'IsActive'},
                {data: 'created_at',name: 'created_at'},
                {data: 'action',name: 'action'}
            ],
            order: [[0, 'desc']]
        });
    });
    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    function changestatus(ele,id,status){
        var error;
        Swal.fire({
            title: 'Are you sure to '+$(ele).data("title")+' this user ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, '+capitalizeFirstLetter($(ele).data("title"))+' it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    headers: {'X-CSRF-Token': Laravel.csrfToken},
                    url: '{{ route("admin.users.changestatus") }}',
                    dataType : 'json',
                    data: {id:id,status:status},
                    success: function(response) {
                        if(response.success == true){
                            Swal.fire(capitalizeFirstLetter($(ele).data("title"))+'ed!','User has been '+$(ele).data("title")+'.','success');
                            if(status=='0'){
                                $(ele).attr('onclick','changestatus(this,'+id+',1);');
                                $(ele).attr('class','btn btn-danger');
                                $(ele).data('title','Active');
                                $(ele).html('InActive');
                            }else{
                                $(ele).attr('onclick','changestatus(this,'+id+',0);');
                                $(ele).attr('class','btn btn-success');
                                $(ele).data('title','InActive');
                                $(ele).html('Active');
                            }
                        }
                    },
                    error: function (jqXHR, status, exception) {
                        if (jqXHR.status === 0) {
                            error = 'Not connected.\nPlease verify your network connection.';
                        } else if (jqXHR.status == 404) {
                            error = 'The requested page not found. [404]';
                        } else if (jqXHR.status == 500) {
                            error = 'Internal Server Error [500].';
                        } else if (exception === 'parsererror') {
                            error = 'Requested JSON parse failed.';
                        } else if (exception === 'timeout') {
                            error = 'Time out error.';
                        } else if (exception === 'abort') {
                            error = 'Ajax request aborted.';
                        } else {
                            error = 'Uncaught Error.\n' + jqXHR.responseText;
                        }
                        Swal.fire('Error!',error,'error');
                    }
                });
            }
        });
    }
    </script>
@endsection