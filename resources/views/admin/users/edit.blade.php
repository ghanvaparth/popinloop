@extends('include.admin_app')
@section('metatitle')
    PopinLoop User
@endsection
@section('content')
<style type="text/css">
    .invalid-feedback{
        display: block;
        font-weight: bold;
    }
</style>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Edit User</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.users.list')}}">User</a></li>
                <li class="breadcrumb-item active">Edit User</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form class="floating-labels m-t-40" method="post" action="{{ route('admin.users.update', $user->id) }}">
                        @csrf
                        <div class="form-group m-b-40">
                            <select class="form-control p-0" name="role" id="role">
                                <option value="">Please select user role</option>
                                @foreach($roles as $role)
                                    <option {{ ($role->id == $user->roles->first()->id)?'selected':'' }} value="{{ $role->id }}"> {{ $role->name }}</option>
                                @endforeach
                            </select>
                            <span class="bar"></span>
                            <label for="role">Role</label>
                            @error('role')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('role') }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group row m-b-40">
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="FirstName" id="FirstName" value="{{ ($user->FirstName)? $user->FirstName : old('FirstName') }}" data-toggle="tooltip" data-placement="bottom" title="First Name">
                                <span class="bar"></span>
                                <label for="FirstName">First Name</label>
                                @error('FirstName')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('FirstName') }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="LastName" id="LastName" value="{{ ($user->LastName)? $user->LastName : old('LastName') }}" data-toggle="tooltip" data-placement="bottom" title="Last Name">
                                <span class="bar"></span>
                                <label for="LastName">Last Name</label>
                                @error('LastName')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('LastName') }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row m-b-40">
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="email" id="email" value="{{ ($user->email)? $user->email : old('email') }}" data-toggle="tooltip" data-placement="bottom" title="Email">
                                <span class="bar"></span>
                                <label for="email">Email</label>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <input type="number" class="form-control" name="Age" id="Age" value="{{ ($user->Age)? $user->Age : old('Age') }}" data-toggle="tooltip" data-placement="bottom" title="Age" min="1" max="150">
                                <span class="bar"></span>
                                <label for="Age">Age</label>
                                @error('Age')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Age') }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row m-b-40">
                            <div class="col-md-6">
                                <select class="form-control p-0" name="Gender" id="Gender">
                                    <option value="">Please select gender</option>
                                    <option value="Male" {{ (old('Gender', $user->Gender)=='Male')?'selected':'' }} >Male</option>
                                    <option value="Female" {{ (old('Gender', $user->Gender)=='Female')?'selected':'' }} >Female</option>
                                </select>
                                <span class="bar"></span>
                                <label for="Gender">Gender</label>
                                @error('Gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Gender') }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <button type="submit" class="btn btn-info waves-effect waves-light m-t-10"> <i class="fa fa-check"></i> Submit</button>
                                            <a href="{{route('admin.users.list')}}" class="btn btn-inverse m-t-10">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection