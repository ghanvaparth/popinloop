@extends('include.admin_app')
@section('metatitle')
    PopinLoop Permission
@endsection

@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Permission List</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.home')}}">Dashboard</a></li>
                <li class="breadcrumb-item active">Permission List</li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
            <div class="d-flex m-t-10 justify-content-end">
                <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                     @if(Auth::user()->can('add-permission'))
                    <a href="{{route('admin.permission.add')}}" class="btn btn-info waves-effect waves-light m-t-10 add-a-tag-color">Add Permission</a>
                    @endif
                </div>
                
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col s12">
            <!-- Column -->
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table striped m-b-20" id="editable-datatable">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    @if(Auth::user()->can('edit-permission'))
                                    @php
                                        $is_role=true;
                                    @endphp
                                    <th>Action</th>
                                    @else
                                    @php
                                        $is_role=false;
                                    @endphp
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{asset('assets/plugins/jquery-datatables-editable/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/media/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('assets/plugins/tiny-editable/mindmup-editabletable.js')}}"></script>
<script src="{{asset('assets/plugins/tiny-editable/numeric-input-example.js')}}"></script>
@if(Session::has('success'))
<script type="text/javascript">
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });
    Toast.fire({
      type: 'success',
      title: '{!! Session::get("success") !!}'
    });
</script>
@endif
<script>
    $(function() {
        var role = '{{$is_role}}';
        var table=$('#editable-datatable').DataTable({
           processing: true,
           serverSide: true,
           ajax:{
                headers: {'X-CSRF-Token': Laravel.csrfToken},
                url:'{!! route("admin.permission.data") !!}',
                type:'POST'
            },
           columns: [
                { data: 'id', name: 'id' },
                { data: 'display_name', name: 'display_name' },
                { data: 'action', name: 'action',visible : role }
            ]
        });
    });
    function delete_confirmation(ele,id){
        swal({   
            title: "Are you sure?",   
            text: "You won't be able to revert this!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            closeOnConfirm: false,
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    headers: {'X-CSRF-Token': Laravel.csrfToken},
                    url: '{{ route("admin.role.destroy") }}',
                    dataType : 'json',
                    data: {id:id},
                    success: function(response) {
                        if(response.succsess == true){
                            Swal.fire(
                              'Deleted!',
                              'Your file has been deleted.',
                              'success'
                            );
                            $('#editable-datatable').DataTable().ajax.reload();
                            redrawDatatable();
                        }
                    },
                    error: function (jqXHR, status, exception) {
                        if (jqXHR.status === 0) {
                            error = 'Not connected.\nPlease verify your network connection.';
                        } else if (jqXHR.status == 404) {
                            error = 'The requested page not found. [404]';
                        } else if (jqXHR.status == 500) {
                            error = 'Internal Server Error [500].';
                        } else if (exception === 'parsererror') {
                            error = 'Requested JSON parse failed.';
                        } else if (exception === 'timeout') {
                            error = 'Time out error.';
                        } else if (exception === 'abort') {
                            error = 'Ajax request aborted.';
                        } else {
                            error = 'Uncaught Error.\n' + jqXHR.responseText;
                        }
                        Swal.fire('Error!',error,'error');
                    }
                });
            }
        });
    }
    function redrawDatatable(){
        $('#editable-datatable').DataTable().ajax.reload();
    }
    </script>
@endsection