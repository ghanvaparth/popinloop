@extends('include.admin_app')
@section('metatitle')
    PopinLoop Permission
@endsection
@section('content')
<style type="text/css">
    .invalid-feedback{
        display: block;
        font-weight: bold;
    }
</style>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Edit Permission</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.permission.list')}}">Permission</a></li>
                <li class="breadcrumb-item active">Edit Permission</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form class="floating-labels m-t-40" method="post" action="{{ route('admin.permission.update', $permission->id) }}">
                        @csrf
                        <div class="form-group m-b-40">
                            <input type="text" class="form-control" name="name" id="name" value="{{ ($permission->display_name)? $permission->display_name : old('name') }}" data-toggle="tooltip" data-placement="bottom" title="Name">
                            <span class="bar"></span>
                            <label for="name">Name</label>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <button type="submit" class="btn btn-info waves-effect waves-light m-t-10"> <i class="fa fa-check"></i> Submit</button>
                                            <a href="{{route('admin.permission.list')}}" class="btn btn-inverse m-t-10">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection