@extends('include.admin_app')
@section('metatitle')
    PopinLoop Role
@endsection
@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Add Role</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.role.list')}}">Role</a></li>
                <li class="breadcrumb-item active">Add Role</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form class="floating-labels m-t-40" method="post" action="{{route('admin.role.store')}}">
                        @csrf
                        <div class="form-group m-b-40">
                            <input type="text" class="form-control" id="input5" name="name" data-toggle="tooltip" data-placement="bottom" title="Name">
                            <span class="bar"></span>
                            <label for="input5">Name</label>
                        </div>
                        <div class="form-group m-b-40">
                            <div class="demo-checkbox">
                                <strong>Permission</strong><br/>
                                @php $i=1; @endphp
                                @foreach($permission as $value)
                                    <input type="checkbox" id="md_checkbox_{{$value->id}}" name="permission[]" class="chk-col-indigo" value="{{$value->id}}" />
                                    <label for="md_checkbox_{{$value->id}}">{{$value->display_name}}</label>
                                    @if($i%4==0)
                                    <br>
                                    @endif
                                    @php $i++; @endphp
                                @endforeach
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <button type="submit" class="btn btn-info waves-effect waves-light m-t-10"> <i class="fa fa-check"></i> Submit</button>
                                            <a href="{{route('admin.role.list')}}" class="btn btn-inverse m-t-10">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection