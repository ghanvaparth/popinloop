@extends('include.admin_app')
@section('metatitle')
    PopinLoop User
@endsection
@section('style')
    <!-- <link href="{{asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet"> -->
    <link href="{{asset('assets/plugins/daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/plugins/dropify/dist/css/dropify.min.css')}}">
<style type="text/css">
    .invalid-feedback{
        display: block;
        font-weight: bold;
    }
        .select2-container--default .select2-selection--multiple {
            border-top: none !important;
            border-left: none !important;
            border-right: none !important;
            border-bottom: 1px solid #d9d9d9 !important;
        }
        .select2-container--default.select2-container--focus .select2-selection--multiple{
            border-top: none !important;
            border-left: none !important;
            border-right: none !important;
            border-bottom: 1px solid #d9d9d9 !important;
        }
        .btn-circle.btn-lg{
            padding: 10px 15px !important;
        }
        .dropify-wrapper .dropify-message p {
            text-align: center !important;
        }
        .dropify-btn{
            margin-bottom: 20px;
        }
        .floating-labels .form-control:focus{
            border-bottom: 1px solid #d9d9d9 !important;
        }
        .container_box{
            border: 1px solid #80808061;
            padding: 20px;
            margin-bottom: 10px;
        }
        .clear-both{
            clear: both;
        }
</style>
@endsection
@section('content')

<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Edit User</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.users.list')}}">User</a></li>
                <li class="breadcrumb-item active">Edit User</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form class="floating-labels m-t-40" method="post" action="{{route('admin.contests.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group m-b-40">
                            <select class="form-control p-0" name="ContestType" id="ContestType">
                                <option value=" ">Please select Contests Type</option>
                                <option value="0" {{ ($ContestData->ContestType=='0' || old('ContestType')=='0') ? 'selected' : '' }}>Audio</option>
                                <option value="1" {{ ($ContestData->ContestType=='1' || old('ContestType')=='1') ? 'selected' : '' }}>Video</option>
                                <option value="2" {{ ( $ContestData->ContestType=='2' || old('ContestType')=='2') ? 'selected' : '' }}>Writing</option>
                            </select>
                            <span class="bar"></span>
                            <label for="ContestType">Contest Type</label>
                            @error('ContestType')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('ContestType') }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group row m-b-40">
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="ContestTitle" id="ContestTitle" data-toggle="tooltip" data-placement="bottom" title="Contest Title" value="{{ ($ContestData->ContestTitle)? $ContestData->ContestTitle : old('ContestTitle') }}">
                                <span class="bar"></span>
                                <label for="ContestTitle">Contest Title</label>
                                @error('ContestTitle')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ContestTitle') }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group m-b-40">
                                <select class="form-control p-0" name="RewardType" id="RewardType">
                                    <option value=" ">Please select Reward Type</option>
                                    <option value="0" {{ ($ContestData->RewardType=='0' || old('RewardType')=='0') ? 'selected' : '' }}>Ruppies</option>
                                    <option value="1" {{ ($ContestData->RewardType=='1' || old('RewardType')=='1') ? 'selected' : '' }}>Point</option>
                                </select>
                                <span class="bar"></span>
                                <label for="RewardType">Reward Type</label>
                                @error('RewardType')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('RewardType') }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="form-group row m-b-40">
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="RewardPoints" id="RewardPoints" data-toggle="tooltip" data-placement="bottom" title="Reward Points" value="{{ ($ContestData->GuestRewardPoints)? $ContestData->GuestRewardPoints : old('RewardPoints') }}">
                                <span class="bar"></span>
                                <label for="RewardPoints">Reward Points</label>
                                @error('RewardPoints')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('RewardPoints') }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row m-b-40">
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="PremiumRewardPoints" id="PremiumRewardPoints" data-toggle="tooltip" data-placement="bottom" title="Premium Reward Points" value="{{ ($ContestData->PremiumRewardPoints)? $ContestData->PremiumRewardPoints : old('PremiumRewardPoints') }}">
                                <span class="bar"></span>
                                <label for="PremiumRewardPoints">Premium Reward Points</label>
                                @error('PremiumRewardPoints')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('PremiumRewardPoints') }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row m-b-40">
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="StartTime" id="StartTime" data-toggle="tooltip" data-placement="bottom" title="Contest Duration" value="{{ ($ContestData->StartTime)? $ContestData->StartTime.' - '.$ContestData->EndTime: old('StartTime') }}">
                                <span class="bar"></span>
                                <label for="StartTime">Contest Duration</label>
                                @error('StartTime')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('StartTime') }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group m-b-40">
                            <select name="has_tags[]" id="has_tags" class="select2 m-b-10 select2-multiple" style="width: 100%" multiple="multiple" data-placeholder="Choose">
                                </select>
                            <span class="bar"></span>
                            @error('has_tags')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('has_tags') }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group row m-b-40">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title"></h4>
                                        <label for="ContestReffrenceImageUrl">Upload Image</label>
                                        <!-- <input type="file" id="ContestReffrenceImageUrl" name="ContestReffrenceImageUrl" class="dropify" /> -->

                                        <input type="file" id="ContestReffrenceImageUrl" name="ContestReffrenceImageUrl" class="dropify" value="{{asset('/uploads/ContestReffrenceImage')}}/{{$ContestData->ContestReffrenceImageUrl}}" data-default-file="{{asset('/uploads/ContestReffrenceImage')}}/{{$ContestData->ContestReffrenceImageUrl}}" />
                                    </div>
                                </div>
                                @error('ContestReffrenceImageUrl')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ContestReffrenceImageUrl') }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row m-b-40">
                            <div class="col-md-6">
                                <div class="demo-checkbox">
                                    <input type="checkbox" id="IsPaid" name="IsPaid" class="filled-in chk-col-light-blue">
                                    <label for="IsPaid">Is Paid</label>
                                    </div>
                            </div>
                        </div>
                        
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <button type="submit" class="btn btn-info waves-effect waves-light m-t-10"> <i class="fa fa-check"></i> Submit</button>
                                            <a href="{{route('admin.users.list')}}" class="btn btn-inverse m-t-10">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<!-- <script type="text/javascript"
     src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
    </script> -->

<script src="{{asset('assets/plugins/moment/moment.js')}}"></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('assets/plugins/select2/dist/js/select2.full.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    $('#StartTime').daterangepicker({
        timePicker: true,
        timePicker24Hour: true,
        timePickerSeconds: true,
        timePickerIncrement: 3000,
        locale: {
                        format: 'YYYY-MM-DD HH:mm:ss'
        }
      });
$(function() {

    $(".select2").select2({
          placeholder: "Select Tags",
            tags: true,
            tokenSeparators: [",", " "],
            ajax:{
              url: "{{route('admin.contests.hastags')}}",
              headers: {'X-CSRF-Token': Laravel.csrfToken},
              dataType: 'json',
              type:"POST",
              data: function(term) {
                  return {
                    q:term.term,
                  };
              },
              processResults: function(data, page) {
                data=data.response;
                  return {
                      results: $.map(data, function(item) {
                          return {
                              text: item.text,
                              id: item.id,
                              isNew:true
                          }
                      })
                  };
              }
            }
        }).on("select2:select", function(e) {
        if(e.params.data.isNew){
            $('#console').append('<code>New tag: {"' + e.params.data.id + '":"' + e.params.data.text + '"}</code><br>');
            $(this).find('[value="'+e.params.data.id+'"]').replaceWith('<option selected value="'+e.params.data.id+'">'+e.params.data.text+'</option>');
        }
});
});
</script>
<script src="{{asset('assets/plugins/dropify/dist/js/dropify.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();
    });
</script>
@endsection